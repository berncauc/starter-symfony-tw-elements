<?php

namespace App\Controller;

use App\Form\PdfType;
use App\Helper\StringHelper;
use setasign\Fpdi\Fpdi;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PdfController extends AbstractController
{
    #[Route('/pdf', name: 'pdf_index')]
    public function index(KernelInterface $kernel, Request $request): Response
    { 

        $form = $this->createForm(PdfType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Récupération du fichier PDF
            $pdfFile = $form->get('pdfFile')->getData();

            // Initilisation des variables
            $pathOuputPdf = $kernel->getProjectDir() .'\\pdf\\';
            $pathOuputPdfExpode= $kernel->getProjectDir() .'\\pdf\\pages\\';
            $filename = StringHelper::getCharactersBeforeChar($pdfFile->getClientOriginalName());
            $fileExtension = StringHelper::getCharactersAfterLastChar($pdfFile->getClientOriginalName());

            // Déplacement du fichier PDF
            $pdfFile->move($pathOuputPdf, $filename ."." .$fileExtension);

            // Traitement de l'explosion du fichier PDF
            $pdfToExplode = new Fpdi();
            $nbrPage = $pdfToExplode->setSourceFile($pathOuputPdf .$filename ."." .$fileExtension);

            for ($page = 1; $page <= $nbrPage; $page++) {
                // Création du fichier PDF
                $pdf = new Fpdi();
                // Ajout d'une page
                $pdf->AddPage();
                $pdf->setSourceFile($pathOuputPdf .$filename ."." .$fileExtension);
                $tplId = $pdf->importPage($page);
                $pdf->useTemplate($tplId);

                // Sauvegarde du fichier PDF
                $pdf->Output($pathOuputPdfExpode .$filename ."_" .$page ."." .$fileExtension, 'F');
            }

            return $this->redirectToRoute('pdf_index');
        }

        return $this->render('pdf/index.html.twig', [
            'form' => $form->createView(),
        ]);

    }

}
