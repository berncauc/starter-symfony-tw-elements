<?php
namespace App\DataFixtures;
ini_set('memory_limit', '512M');

use Faker\Factory;
use App\Entity\Diplome;
use App\Entity\Matiere;
use App\Entity\Apprenant;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // Datas
        $diplomesListe = [
            'Baccalauréat général',
            'Baccalauréat technologique',
            'Baccalauréat professionnel',
            'Licence',
            'Master',
            'Doctorat',
            'BTS (Brevet de Technicien Supérieur)',
            'DUT (Diplôme Universitaire de Technologie)',
            'DEUST (Diplôme d\'Études Universitaires Scientifiques et Techniques)',
            'DEUG (Diplôme d\'Études Universitaires Générales)',
            'CAP (Certificat d\'Aptitude Professionnelle)',
            'BEP (Brevet d\'Études Professionnelles)',
            'BT (Brevet de Technicien)',
            'DMA (Diplôme des Métiers d\'Art)',
            'DNMADE (Diplôme National des Métiers d\'Art et du Design)',
            'DCG (Diplôme de Comptabilité et de Gestion)',
            'DSCG (Diplôme Supérieur de Comptabilité et de Gestion)',
            'BTS Tourisme (Brevet de Technicien Supérieur en Tourisme)',
            'Diplôme d\'Ingénieur',
            'MBA (Master of Business Administration)'
        ];

        $matieresListe = [
            'Mathématiques','Physique','Chimie','Biologie','Histoire','Géographie',
            'Langues étrangères','Français','Philosophie','Informatique','Économie',
            'Droit','Management','Comptabilité','Marketing','Finance','Ressources humaines',
            'Statistiques','Sociologie','Psychologie','Anthropologie','Art','Musique',
            'Théâtre','Danse','Architecture','Design','Journalisme','Communication',
            'Santé','Médecine','Pharmacie','Soins infirmiers','Kinésithérapie','Ingénierie',
            'Électricité','Mécanique','Génie civil','Génie informatique','Génie chimique',
            'Environnement','Agriculture','Biotechnologie','Tourisme','Hôtellerie','Cuisine',
            'Sport','Education physique','Enseignement','Marketing digital','E-commerce'
        ];

        $faker = Factory::create('fr_FR');

        $apprenants = new ArrayCollection();
        $matieres = new ArrayCollection();

        // Création de 1000 apprenants
        for ($j = 0; $j < 1000; $j++) {
            $apprenant = new Apprenant();
            $apprenant->setNom($faker->lastName());
            $apprenant->setPrenom($faker->firstName());
            $apprenants->add($apprenant);
            $manager->persist($apprenant);
        }

        // Création de 50 matières
        for ($j = 0; $j < 50; $j++) {
            $matiere = new Matiere();
            $matiere->setNom($matieresListe[$j]);
            $matieres->add($matiere);
            $manager->persist($matiere);
        }

        // Création de 10 diplomes
        $diplomesPlacer = new ArrayCollection();
        for ($i = 0; $i < 10; $i++) {            

            $diplome = new Diplome(); 

            $diplomeName = $diplomesListe[rand(0, count($diplomesListe) - 1)];
            while($diplomesPlacer->contains($diplomeName)){
                $diplomeName = $diplomesListe[rand(0, count($diplomesListe) - 1)];
            }

            $diplome->setNom($diplomeName);
            $diplomesPlacer->add($diplomeName);

            // Ajout de 0 à 20 apprenants dans le diplome
            $apprenantsPlacer = new ArrayCollection();
            for($j = 0; $j < rand(15,20); $j++){

                $apprenant = $apprenants->get(rand(0, 99));
                while($apprenantsPlacer->contains($apprenant)){
                    $apprenant = $apprenants->get(rand(0, 99));
                }

                $diplome->addApprenant($apprenant);
                $apprenantsPlacer->add($apprenant);

            }

            // Ajout de 6 à 10 matières dans le diplome
            $matieresPlacer = new ArrayCollection();
            for($j = 0; $j < rand(6,10); $j++){ 

                $matiere = $matieres->get(rand(0, 49));
                while($matieresPlacer->contains($matiere)){
                    $matiere = $matieres->get(rand(0, 49));
                }

                $diplome->addMatiere($matiere);
                $matieresPlacer->add($matiere);
            }

            $manager->persist($diplome);
        }

        $manager->flush();
    }
}

